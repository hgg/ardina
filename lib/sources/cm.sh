
function extension {
    echo "pdf"
}

function get_first_page {
    # $1 - year
    # $2 - month
    # $3 - day
    # https://www.cmjornal.pt/mais-cm/capas/detalhe/cm-de-hoje-02102019?ref=Capas_Capa
    # //cdn.cmjornal.pt/files/2019-10/2019-10-02_03_43_11_Capa_CM_02_de_outubro.pdf
    local file=$4
    local date=${3}${2}${1}

    # Find the first page URL
    URL=`wget \
            --quiet \
            --output-document - \
            https://www.cmjornal.pt/mais-cm/capas/detalhe/cm-de-hoje-${date}?ref=Capas_Capa | \
                grep \
                    --only-matching \
                    --ignore-case \
                    --perl-regexp \
                    --regexp '//cdn.cmjornal.pt/files/.*\.pdf' | head --lines 1`
    URL="https:${URL}"

    # Get the cover page
    download_file $file $URL
}
