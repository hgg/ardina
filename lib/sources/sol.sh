
function get_first_page {
    # $1 - year
    # $2 - month
    # $3 - day
    # https://capas.newsplex.pt/capas/capa_jornal_sol_07_09_2019.jpg
    file=$4

    # Check if there is a new cover today (only on saturdays)
    check_days 1 2 3 4 5 7

    # Find the first page URL
    URL="https://capas.newsplex.pt/capas/capa_jornal_sol_${3}_${2}_${1}.jpg"

    # Get the cover page
    download_file $file $URL
}
