
function get_first_page {
    # $1 - year
    # $2 - month
    # $3 - day
    # https://capas.newsplex.pt/detalhe/capa_jornal_i_03_10_2019.jpg
    file=$4

    # Check if there is a new cover today (no new covers on saturdays)
    check_days 6 7

    # Find the first page URL
    URL="https://capas.newsplex.pt/capas/capa_jornal_i_${3}_${2}_${1}.jpg"

    # Get the cover page
    download_file $file $URL
}
