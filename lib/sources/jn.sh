
function get_first_page {
    # Jornal de Notícias: https://www.jn.pt/edicao-impressa.html
    file=$4

    # Only the current date first page can be downloaded
    only_current_date

    # Get the cover page
    URL='https://static.globalnoticias.pt/jn//image.aspx?brand=JN&type=capa'
    download_file $file $URL
}
