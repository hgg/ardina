#
# Download publico's first page

function get_first_page {
    # https://imagens.publicocdn.com/imagens.aspx/2019/06/14/P1/publico.jpg?tp=ARQUIVO
    year=$1
    month=$2
    day=$3
    file=$4

    URL="https://imagens.publicocdn.com/imagens.aspx/$year/$month/$day/P1/publico.jpg?tp=ARQUIVO"
    download_file $file $URL
}
