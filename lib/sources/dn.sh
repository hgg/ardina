
function get_first_page {
    # https://www.dn.pt/edicao-do-dia.html
    year=$1
    month=$2
    day=$3
    file=$4

    # Only the current date first page can be downloaded
    only_current_date

    # Check if there is a new cover today (no new covers on sundays)
    check_days 7

    # Find the first page URL
    URL=`wget \
            --quiet \
            --output-document - \
            --post-data 'op=Newspaper&type=editions' \
            --header 'Referer: https://www.dn.pt/edicao-do-dia.html' \
            https://www.dn.pt/ajax/requests.aspx | \
                grep \
                    --only-matching \
                    --perl-regexp \
                    --regexp 'https://static.*?(?=\\\\")' | \
                    head --lines 1`

    # Get the cover page
    download_file $file $URL
}
