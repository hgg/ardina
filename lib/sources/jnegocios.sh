function extension {
    echo "pdf"
}

function get_first_page {
    # $1 - year
    # $2 - month
    # $3 - day
    file=$4

    # Only the current date first page can be downloaded
    only_current_date

    # Check if there is a new cover today (only on week days)
    check_days 6 7

    # Find the first page URL
    URL=`wget \
            --quiet \
            --output-document - \
            https://www.jornaldenegocios.pt/mais/primeira-pagina | \
                grep \
                    --only-matching \
                    --perl-regexp \
                    --regexp '//cdn\.jornaldenegocios\.pt/files/.*\.PDF'`
    URL="https:${URL}"

    # Get the cover page
    download_file $file $URL
}
