#!/bin/bash

function get_front_page {
    file_name=$(./ardina.sh -s ${1})
}

SOURCES="dn jn publico cm jornal-i sol"
ARCHIVE=../archive/

YEAR=`date +"%Y"`
MONTH=`date +"%m"`
DAY=`date +"%d"`
BASE_DIR=${ARCHIVE}${YEAR}/${MONTH}/${DAY}

for source in $SOURCES
do
    get_front_page $source
done
gm montage -tile 3x10 -geometry 640x $BASE_DIR/*.jpg $BASE_DIR/0000_montage.jpg
