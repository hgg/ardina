#!/usr/bin/env bash

# Config
SOURCES=../lib/sources/
ARCHIVE=../archive/
DATE=`date +"%Y-%m-%d"`
VIEWER=xdg-open

function only_current_date {
    CUR_DATE=`date +"%Y-%m-%d"`
    if [ "$DATE" != "$CUR_DATE" ]
    then
        echo "${CURRENT_SOURCE}: This source only allows current date" >&2
        exit 1
    fi
}

# Checks if the date is on a forbiden day
function check_days {
    weekday=`date -d $DATE +"%u"`
    for forbiden_day in "$@"
    do
        if [ "$forbiden_day" == "$weekday" ]
        then
            WEEKDAY=`date -d $DATE +"%A"`
            echo "${CURRENT_SOURCE}: No cover page on $WEEKDAY" >&2
            exit 1
        fi
    done
}

# Download file
function download_file {
    wget --quiet --output-document="$1" --max-redirect=0 $2
    if [ $? != 0 ]
    then
        # House keeping
        rm "$1"
        rmdir ${ARCHIVE}${YEAR}/${MONTH}/${DAY} 2> /dev/null
        echo "${CURRENT_SOURCE}: Error downloading file $2" >&2
        exit 1
    fi
}

# Default extension
function extension {
    echo "jpg"
}

# Download a source to the $ARCHIVE directory
function download_source {
    # source the download function
    local SOURCE=${SOURCES}/${1}.sh
    if [ -e $SOURCE ]
    then
        source $SOURCE
        if [ ! "$(type -t get_first_page)" = "function" ]
        then
            echo "The function \"get_first_page\" isn't defined in source ${1}" >&2
            exit 1
        fi
    else
        echo "No such source: ${1}." >&2
        exit 1
    fi
    # Breakup the date
    YEAR=`date -d $DATE +"%Y"`
    if [ $? != 0 ]
    then
        echo "Invalid date: $DATE" >&2
        exit 1
    fi
    MONTH=`date -d $DATE +"%m"`
    DAY=`date -d $DATE +"%d"`
    FILE=${ARCHIVE}${YEAR}/${MONTH}/${DAY}/${YEAR}-${MONTH}-${DAY}_${1}.$(extension)
    mkdir -p ${ARCHIVE}${YEAR}/${MONTH}/${DAY}
    if [ -e $FILE ]
    then
        echo "File exists ${YEAR}-${MONTH}-${DAY}_${1}.$(extension)" >&2
        exit 1
    fi
    # Download the first page
    get_first_page $YEAR $MONTH $DAY $FILE

    # Convert to jpg when needed and resize the message
    if [ $(extension) == "pdf" ]
    then
        gm convert -resize 1920 $FILE ${FILE%.pdf}.jpg
        rm $FILE
        FILE=${FILE%.pdf}.jpg
    elif [ $(extension) == "jpg" ]
    then
        gm mogrify -resize 1920 $FILE
    else
        echo "Unknown extension: $(extension)" >&2
        exit 1
    fi
}


function show_file {
    if [ ! -z ${PREVIEW+x} ]
    then
        $VIEWER ${1}
    fi
}


# Help
function show_help {
    cat << EOF
Usage: $0 [OPTIONS] <COMMAND>

OPTIONS:
    -d <yyyy-mm-dd>  Set the date
    -p               Open the downloaded image

COMMANDS:
    -h           This help
    -s <source>  Downloads the <source>
EOF
}

while getopts ":hs:d:p" opt
do
    case "$opt" in
        # OPTIONS
        d)
            DATE=$OPTARG
            ;;
        p)
            PREVIEW=1
            ;;
        # COMMANDS
        h)
            show_help
            exit 0
            ;;
        s)
            CURRENT_SOURCE=$OPTARG
            download_source $CURRENT_SOURCE $DATE
            show_file $FILE
            echo $FILE
            exit 0
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            show_help
            exit 1
            ;;
        :)
            echo "Option -$OPTARG needs an argument" >&2
            exit 1
            ;;
    esac
done

echo "Necessita de indicar uma opção." >&2
echo >&2
show_help
exit 1
