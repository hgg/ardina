# Intro

This package downloads, for a given day, the front pages of the configured
sources (right now only portuguese newspapers).

# Usage

    ardina [OPTIONS]

# Requirements

* GraphicsMagick
* imgur-uploader (python package, install with pip3)

# Sources

* Público: https://imagens.publicocdn.com/imagens.aspx/2019/06/14/P1/publico.jpg?tp=ARQUIVO
* Diário de Notícias: https://www.dn.pt/edicao-do-dia.html
* Jornal de Notícias: https://www.jn.pt/edicao-impressa.html

* Correio da Manhã: https://cdn.cmjornal.pt/files/2019-06/2019-06-15_02_38_46_Capa_CM_15_de_Junho.pdf
* Jornal Económico: https://leitor.jornaleconomico.pt/
* Sol: https://capas.newsplex.pt/capas_pdf/capa_jornal_sol_15_06_2019.pdf
* Jornal i: https://capas.newsplex.pt/capas_pdf/capa_jornal_i_14_06_2019.pdf
* Destak: http://www.destak.pt/edicao-dia
* Visao: https://visao.sapo.pt/revistas/2019-06-12-Edicao-1371
* Sábado: do leitor da Cofina: https://i.prcdn.co/img?file=ee082019061200000051001001&page=1&scale=355
